<%@ page session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <c:set var="serverName" value="${pageContext.request.serverName}" />
  <c:set var="contextPath" value="${pageContext.servletContext.contextPath}"/>
  <c:set var="baseUrl" value="${serverName}${contextPath}" />
  <script src="//js.arcgis.com/3.12/"></script>
  <script src="https://${baseUrl}/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    
  
  <link href="//js.arcgis.com/3.12/esri/css/esri.css" rel="stylesheet" type="text/css"></link>
  <link href="//js.arcgis.com/3.12/dijit/themes/claro/claro.css" rel="stylesheet" type="text/css" ></link>
  <link href="https://${baseUrl}/css/map.css" rel="stylesheet" type="text/css" ></link>
  <script src="https://${baseUrl}/js/map.js" type="text/javascript"></script>
  <script type="text/javascript">
  
  function isUrlForServer(url) {
      var ourHost = '${serverName}';
      return (url.indexOf('http://' + ourHost) == 0 || url.indexOf('https://' + ourHost) == 0);
  }
  
  $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
      if (jQuery.support.cors) {
        // Turn relative URLs into fully qualified URLs that use https
        var thisUrl = options.url;
        if ((thisUrl.lastIndexOf('http://', 0) !== 0) && (thisUrl.lastIndexOf('https://', 0) !== 0)) {
          thisUrl = "https://${serverName}" + thisUrl;
          options.url = thisUrl;
        }
        // Ensure we send credentials for https requests back to this server.
        if (isUrlForServer(options.url)) {
            if(!(options.xhrFields)) {
              options.xhrFields = {};
            }
            options.xhrFields.withCredentials = true;
        }
      }
    });
  
  $(document).ready(
      function() {
        $.get('https://${baseUrl}/mapSecurityCheck.json', function(data) {
            if (data.loggedIn) {
                if (data.allowed) {
                    $.get("https://${baseUrl}/restricted/map-body.jsp", function(data) {
                       $("#mainBody").html(data);
                       dojo.addOnLoad(initMapViewer);
                    }); 
                } 
                else {
                    $.get("https://${baseUrl}/restricted/not-authorized-body.jsp", function(data) {
                       $("#mainBody").html(data);
                    }); 
                }
            } else {
                window.location = 'https://${baseUrl}/restricted/map-login.jsp';
          }
        });
      });
  
  </script>
</head>
<body id="mainBody">
</body>
</html>