<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="mainContent">
  <div id="header">
    <div id="nav-wrapper">
      <div id="nav">
        Hello, <c:out value="${pageContext.request.userPrincipal.name}" /> | 
        <a href="${pageContext.request.contextPath}/">Home</a> |
        <a href="${pageContext.request.contextPath}/logout">Log out</a>
      </div>
      &nbsp;
    </div>
  </div>
  <div id="mapViewerDiv"></div>
</div>