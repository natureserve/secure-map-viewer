<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
Sorry, <c:out value="${pageContext.request.userPrincipal.name}" />. You are not authorized to view this page.
<br/><br/>
Maybe you want to <a href="${pageContext.request.contextPath}/logout">log out</a> and try again with a different account?
