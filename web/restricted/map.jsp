<%@ page session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <c:set var="contextPath" value="${pageContext.servletContext.contextPath}"/>
  <script src="//js.arcgis.com/3.12/"></script>
  <script src="${contextPath}/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    
  <link href="//js.arcgis.com/3.12/esri/css/esri.css" rel="stylesheet" type="text/css"></link>
  <link href="//js.arcgis.com/3.12/dijit/themes/claro/claro.css" rel="stylesheet" type="text/css" ></link>
  <link href="${contextPath}/css/map.css" rel="stylesheet" type="text/css" ></link>
  <script src="${contextPath}/js/map.js" type="text/javascript"></script>
  <script type="text/javascript">
  
  $(document).ready(
      function() {
        dojo.addOnLoad(initMapViewer);
      });
  
  </script>
</head>
<body id="mainBody">
  <div id="mainContent">
  <div id="header">
    <div id="nav-wrapper">
      <div id="nav">
        Hello, <c:out value="${pageContext.request.userPrincipal.name}" /> | 
        <a href="${pageContext.request.contextPath}/">Home</a> |
        <a href="${pageContext.request.contextPath}/logout">Log out</a>
      </div>
      &nbsp;
    </div>
  </div>
  <div id="mapViewerDiv"></div>
</div>
</body>
</html>