<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Login Error</title>
</head>
<body>
    <c:url var="url" value="/index.jsp"/>
    <h3>Invalid user name or password.</h3>

    <p>Click here to <a href="${url}">try Again</a></p>
</body>
</html>