  dojo.require("dojo.NodeList-traverse");
  dojo.require("dojo.NodeList-manipulate");
  dojo.require("dojox.collections.ArrayList");
  dojo.require("dijit.layout.AccordionContainer");
  dojo.require("dijit.layout.BorderContainer");
  dojo.require("dijit.layout.ContentPane");
  dojo.require("dijit.TitlePane");
  dojo.require("esri.map");
  dojo.require("esri.geometry");
  dojo.require("esri.tasks.geometry");
  dojo.require("esri.tasks.query");
  dojo.require("esri.layers.agsdynamic");
  dojo.require("esri.layers.FeatureLayer");
  dojo.require("esri.dijit.Legend");
  dojo.require("esri.dijit.AttributeInspector");
  dojo.require("esri.dijit.BasemapGallery");
  dojo.require("esri.dijit.OverviewMap");
  dojo.require("esri.arcgis.utils");

  
  var AGS_SERVER="http://maps.natureserve.org/pub1/rest/services/Species_Counts/NatureServe_Species_of_Conservation_Concern_by_County/MapServer";
  var FEATURE_LAYER_NAME = 'Species Counts by County';
  var FEATURE_LAYER_FIELDS = ["NAME", "STATE_NAME", "G12_ESA"];
  var FEATURE_LAYER_FOOTNOTES = {"G12_ESA": "U.S. ESA Listed, Proposed, Candidate and NatureServe Imperiled (G1-G2) Species"};
  var FEATURE_LAYER_IDENTIFY_WINDOW_TITLE = 'County Details';
  
  var FEATURE_LAYER_LEGEND_TITLE = 'Number of Species of Conservation Concern';
  var LEGEND_TITLE = 'Legend';
  var MAP_TITLE = 'Number of Species of Conservation Concern Per County';
  
  var DATA_REFRESH_DATE = "March 2014";
  
  var HEADER_DIV = 'headerDiv';
  var MAP_VIEWER_DIV = 'mapViewerDiv';
  var BASE_MAP_URLS = ["http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer"];
  
  var BASE_MAP_SWITCHER_TITLE = 'Switch Basemap';
  
  var TARGET_DOJO_THEME = "claro";
   
  var map;
  var mapLayerId = 'allFeatures';
  var baseLayerId = 'basemap';
  var featureLayerId = 'selectedFeatures';
  var overlayLayerId = 'annotations';
  
  function showError(error) {
    alert('Error! - ' + error.name + ', ' + error.message);
  }
  
  function getFieldDefinition(fieldName, fieldDefinitions) {
    var fieldNameUpper = fieldName.toUpperCase();
    for (var i = 0; i < fieldDefinitions.length; ++i) {
	  if (fieldDefinitions[i].name.toUpperCase() == fieldNameUpper) {
	    return fieldDefinitions[i];
	  }
    }
	return null;
  }
  
  function createAttributeTable(graphic, featureLayer, featureFields) {

    var html = "<div>";
	html += "<table>";
    var footnotes = {};
    var curFootnote = 1;
	for (var fieldIndex = 0; fieldIndex < featureFields.length; ++fieldIndex) {
	  var field = getFieldDefinition(featureFields[fieldIndex], featureLayer.fields);
	  if (field != null) {
		  html += "<tr><th valign='top' align='right' width='50%'>";
		  var label = field.alias ? field.alias : field.name;
		  html += label;
          if (FEATURE_LAYER_FOOTNOTES[field.name]) {
            html += "<span style='font-size:70%;vertical-align:super;font-weight:normal'>" + curFootnote + "</span>";
            footnotes[curFootnote] = FEATURE_LAYER_FOOTNOTES[field.name];
            ++ curFootnote;
          }
		  html += ":</th><td>";
		  var value = graphic.attributes[field.name];
		  html += "" + value + "</td></tr>";
	  }
	}
	html += "</table>";
	
    if (curFootnote > 1) {
       html += "<p/>";
      for (var footnoteIndex = 1; footnoteIndex < curFootnote; ++footnoteIndex) {
        html += "<span style='font-size:70%;vertical-align:super'>" + footnoteIndex + "</span> " + footnotes[footnoteIndex] + "<br/>";
      }
    }
	html += "</div>";
	return html;
  }
  
  function queryMap(evt) {
    var query = new esri.tasks.Query();
			query.geometry = evt.mapPoint;
			query.spatialRelationship = esri.tasks.Query.SPATIAL_REL_INTERSECTS;
			var featureLayer = map.getLayer(featureLayerId);
			featureLayer.selectFeatures(query, esri.layers.FeatureLayer.SELECTION_NEW, function(features, selectionMethod) {
			   if (features.length > 0) {
			       var attributeTable = createAttributeTable(features[0], featureLayer, FEATURE_LAYER_FIELDS);
                   map.infoWindow.setTitle(FEATURE_LAYER_IDENTIFY_WINDOW_TITLE);
                   map.infoWindow.setContent(attributeTable);
                   
                   dojo.query('.infowindow .content').first().style({ 'overflow': 'auto'});
                   map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));         
			   } else {
                 map.infoWindow.hide();
               }
			},
			showError);
  }
  
  
  function handleLayersAdded(results) {
    var featureLayer = map.getLayer(featureLayerId);
    dojo.query('#copyrightTextSpan').innerHTML(featureLayer.copyright);
	
	var legend = new esri.dijit.Legend({
	  layerInfos: [{layer: featureLayer, title: FEATURE_LAYER_LEGEND_TITLE}], map: map }, "legendDiv");
	legend.startup();
	
	dojo.connect(map, "onClick", queryMap);
	dojo.connect(map.infoWindow, "onHide", function() {
          featureLayer.clearSelection();
        });
  }
  
  function addFeatureLayers(resp) {
      // First, find the index of the target feature layer that we will work with
	  var featureLayerIndex = -1;
	  for (var i = 0; i < resp.layers.length; ++i) {
		var layer = resp.layers[i];
		if (layer.name == FEATURE_LAYER_NAME) {
		  featureLayerIndex = layer.id;
		} 
	  }
	  // Next, add the appropriate layers to the map.
	  var basemapURL = BASE_MAP_URLS[0];
      var basemap = new esri.layers.ArcGISTiledMapServiceLayer(basemapURL, {id: baseLayerId});
	  
	  var imageParams = new esri.layers.ImageParameters();
		imageParams.layerIds = [featureLayerIndex];
		imageParams.layerOption = esri.layers.ImageParameters.LAYER_OPTION_SHOW;

		var mapLayer = new esri.layers.ArcGISDynamicMapServiceLayer(AGS_SERVER, 
		{ id: mapLayerId, imageParameters: imageParams});
		mapLayer.opacity = 0.6;
		
		var featureLayer = new esri.layers.FeatureLayer(
		    AGS_SERVER + "/" + featureLayerIndex, 
			{ id: featureLayerId,
              outFields: ["*"], 
			  mode: esri.layers.FeatureLayer.MODE_SELECTION});
		var selectionSymbol = new esri.symbol.SimpleFillSymbol().setColor(new dojo.Color([255,255,0,0.5]));
		featureLayer.setSelectionSymbol(selectionSymbol);
		
		dojo.connect(map, "onLayersAddResult", handleLayersAdded);
		map.addLayers([basemap, mapLayer, featureLayer]);

		// 50 pixels is a buffer for the lead-lines
		//var targetHeight = (map.height / 2) - 50;
		//var targetWidth = (map.width / 2);// - 12;
		var targetHeight = 200;
		var targetWidth = 300;
        map.infoWindow.resize(targetWidth, targetHeight);
  }
  
  function resizeMapViewer() {
      var viewport = dijit.getViewport(); 
      var targetHeight = viewport.h - 40;
      var targetWidth = viewport.w - 30; 
      
      dojo.query('#' + MAP_VIEWER_DIV).first().style({width: targetWidth + "px", height: targetHeight + "px", margin: '0px', border: '0px', padding: '0px'});
  }
  
  
  function initMapViewer() {
	if (dojo.hasClass(dojo.body(), TARGET_DOJO_THEME) == false) {
	  dojo.addClass(dojo.body(), TARGET_DOJO_THEME);
	}
	
	resizeMapViewer();
	dojo.connect(window, "onresize", resizeMapViewer);
	
	var contentContainer = new dijit.layout.BorderContainer({design:"sidebars", gutters:"false"}, MAP_VIEWER_DIV);
	var rightPane = new dijit.layout.ContentPane({region:"right", style:"width:145px;border:0px;padding:0px;margin:0px"});
	var rightAccordian = new dijit.layout.AccordionContainer();
	var legendPane = new dijit.layout.ContentPane({title: LEGEND_TITLE, id:"legendPane"});
	legendPane.set('content', "<div id='legendDiv'/>");
	rightAccordian.addChild(legendPane);
	rightPane.set('content', rightAccordian);
	contentContainer.addChild(rightPane);
	
	var centerPane = new dijit.layout.ContentPane({region:"center", style:"border:0px;padding:0px;margin:0px"});
	var centerAccordian = new dijit.layout.AccordionContainer();
	var mapPane = new dijit.layout.ContentPane({title: MAP_TITLE, id:"mapPane", style:"border:0px;padding:0px;margin:0px"});
	mapPane.set('content', "<div id='mapDiv' style='width:100%;height:100%'></div><div id='basemapSwitcherContainerDiv' style='position:absolute; right:30px; top:0px; z-Index:999;'><div id='basemapSwitcherDiv'></div></div><div id='logoDiv' style='position:absolute;left:10px;bottom:10px;z-index:25'><div class='ns-map-logo'></div><span id='copyrightTextSpan'>Data last updated " + DATA_REFRESH_DATE + "</span></div>");
	centerAccordian.addChild(mapPane);
	centerPane.set('content', centerAccordian);
	contentContainer.addChild(centerPane);
	
	contentContainer.startup();
	
  
	var startExtent = new esri.geometry.Extent({
	  "xmin":-115.0,
	  "ymin":30.0,
	  "xmax":-75.0,
	  "ymax":47.0,
	  "spatialReference":{"wkid":4326}
	});
	
	var projectedStartExtent = esri.geometry.geographicToWebMercator(startExtent);
	map = new esri.Map("mapDiv", { extent: projectedStartExtent, fitExtent: true , wrapAround180:true});
	

	dojo.io.script.get({
                url:AGS_SERVER,
                content:{f:'json'},
                load: addFeatureLayers,
				jsonp:'callback'
        });
	
  }
  
  
	
  