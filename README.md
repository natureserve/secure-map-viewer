# Status Of Project #

This sample project is obsolete. It is being left available for historical reference purposes only.

Browser security practices have advanced since this project was created in 2015, and most browsers now treat requests from an http URL (http://foo.example) to an https version of the same URL (https://foo.example) as a cross-site request. Changes to the treatment of SameSite cookies in late 2020/early 2021 have removed the ability for an http page to make an https request to the same domain in a way that includes secured session cookies. The approach demonstrated within this project will no longer work in most modern browsers.

# Purpose #

This is an example project to demonstrate a solution for an application with the following requirements.

* Online map viewer using Esri Javascript API
* Map viewer is secured, and only available for certain users
* Map viewer must include a map service that is only available over http

# Building and Running #

This was developed using Eclipse Luna, Tomcat 7, Apache 2.4, and Java 7. It can be run through Eclipse. However, things are simple enough that it should be fairly easy to get running under other Java IDEs and web containers.

## Tomcat / Web Container Setup ##

Logins are handled through a container managed security realm. The web container must define a security realm named UserDatabase that supports two roles:

* authenticated - available for all logged in users
* map - grants permission to use the map viewer

The serverFiles/tomcat-users.xml is a configuration file that can be used by Tomcat's memory realm for this purpose.

The application should be run using the context path /secure-map/viewer and be accessible over AJP port 8009. The web application itself doesn't rely on these specific values, but if you choose a different path or port, you will need to update the Apache configuration to match.

## Apache / Web Server Setup ##

A web server must be configured to provide http and https access using a fully qualified hostname, though you can use your hosts file to cheat if desired.

The serverFiles/httpd.conf file is a configuration file for Apache 2.4. You will need to configure it to use your own host name and SSL certificate. A self-signed certificate should work. 
