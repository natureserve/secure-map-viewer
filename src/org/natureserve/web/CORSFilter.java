package org.natureserve.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Filter that handles Cross Origin Resource Sharing (CORS) handling.  It has two jobs:
 * 
 * 1. Handles all OPTIONS requests.  These are received when 'pre-flight requests' are issued by a browser
 * which is attempting to confirm cross-domain requests are allowed to this server.
 * 
 * Ideally, these would be handled in the Jersey service classes, but the OPTIONS processing which should occur implicitly is broken.
 * Rather than hand code specific OPTIONS request handlers for each service method, this filter takes over this job globally, allowing
 * all requests to succeed.  In the main, this allows 'cross-domain' requests from http://biotics.etc to httpS://biotics.etc
 * 
 * 2. Sets the Access-Control-Allow-Origin header for all responses.  A pre-flight request may succeed, but in order for the actual
 * request to not be subsequently rejected, this header must allow the origin specified in the actual request's ORIGIN header.
 *  
 * @author frank_mclean@natureserve.org
 */
public class CORSFilter implements Filter {

    @SuppressWarnings("unused")
    private FilterConfig filterConfig = null;

    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }
    
    /*
    For cross-domain 'preflight requests' an OPTIONS request is issued.
    --------- From JAX-RS spec Nov 1, 2011 , section 3.3.5 "HEAD and OPTIONS" ---------

    HEAD and OPTIONS requests receive additional automated support. On receipt of a HEAD request an implementation MUST either:
    1. Call a method annotated with a request method designator for HEAD or, if none present,
    2. Call a method annotated with a request method designator for GET and discard any returned entity.
    Note that option 2 may result in reduced performance where entity creation is significant.
    On receipt of an OPTIONS request an implementation MUST either:
    1. Call a method annotated with a request method designator for OPTIONS or, if none present,
    2. Generate an automatic response using the metadata provided by the JAX-RS annotations on the matching  class and its methods.

    ----------- End excerpt from JAX-RS spec --------

    Without an explicit @OPTIONS method, the 'automatic response' is an exception filled (see Tomcat log) bunch of garbage.  And path
    matching is such that a 'generic' OPTIONS method will be passed over for a more targeted GET method.  Or, in fact, the equivalent
    generic GET method.  Which basically means you have to code an OPTIONS method with the _exact_ same path you're trying to respond
    to pre-flight for, in order to jump onto and respond to it appropriately.
    */

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        
        String page = req.getRequestURI();
        
        // Add CORS response headers across the board.
        if (req.getHeader("Origin") != null) {
            // TODO: Nail this down to a specific domain properly | Allow any source for request.
            //resp.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
            
            // Only allow requests made by pages that originated from the same server on which this
            // application is running
            resp.setHeader("Access-Control-Allow-Origin", "http://" + req.getServerName());
            // Allow all the headers the client has asked to be allowed.
            resp.setHeader("Access-Control-Allow-Headers", req.getHeader("Access-Control-Request-Headers"));
            // Allow these methods
            resp.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            // Make sure cookies can be passed cross-domain, otherwise we will lose session.  Note that if the allow origin header is set to *,
            // then the following always acts as false.
            resp.setHeader("Access-Control-Allow-Credentials", "true");
        }
        
        if (!("OPTIONS".equals(req.getMethod()))) {
            // Not OPTIONS - process as normal.
            chain.doFilter(request, response);
        }
        else {
            // OPTIONS - return to caller without further ado.
            ; // do nothing
        }
    
    }
    
}

