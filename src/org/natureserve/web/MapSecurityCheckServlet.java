package org.natureserve.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Web service that returns a JSON object indicating whether the user is
 * logged in and authorized to view the map viewer.
 * 
 * @author dave_hauver@natureserve.org
 */
public class MapSecurityCheckServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean isAllowed = request.isUserInRole("map");
        boolean isLoggedIn = request.getUserPrincipal() != null;
        response.setContentType("application/json");        
        // Don't allow responses to be cached
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        PrintWriter writer = response.getWriter();
        writer.print("{\"allowed\": " + isAllowed + ", \"loggedIn\": " + isLoggedIn + "}");
    }
    

}
